import {Col, Card, Row, Button, Accordion} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';


export default function OrderCard ({ordersProp})
{
	console.log(ordersProp)
	//destricturing the products prop
	const {_id, orderStatus, purchasedOn, totalAmount, products} = ordersProp;
	
	//console.log(products[0].name)
	console.log(products)

	const listItems = products.map((d) => <li key={d.name}>Item Name: {d.name} (Qty: {d.quantity}) </li>);

	
	

	return (
			<center>
			<Col xs={12} lg={8}>
				<Accordion defaultActiveKey="0">
				    <Accordion.Item eventKey={_id}>
				    <Accordion.Header>
				    ORDER NUMBER:  <strong className="d-flex ps-3">{_id}</strong>
				    </Accordion.Header>
				    				   
				    	<Accordion.Body>
				    	<div className="align-items-left">Total Amount: <strong>{totalAmount}</strong></div>
				       	<div className="align-items-left">Order Status: <strong>{orderStatus}</strong></div>
				       	<div className="align-items-left">Placed on: <strong>{purchasedOn.slice(0, purchasedOn.length - 14)}</strong></div>
				       		<Accordion flush>
				       			<Accordion.Header>Items:</Accordion.Header>
				       			<Accordion.Body>

				       				<div>
				       					{listItems}
				       				</div>
				       				
				       			</Accordion.Body>
				       		</Accordion>
				       	</Accordion.Body>
				    </Accordion.Item>
				</Accordion>
				
			</Col>
			</center>
			
		);
}