// IMPORTS
import {Link, NavLink,} from 'react-router-dom';
import {Button, InputGroup, Badge} from 'react-bootstrap';

import {Container, Row, Col, Form, Nav, Navbar, NavDropdown} from 'react-bootstrap';

import {useContext, useState, useEffect } from 'react'; // HOOK  
import UserContext from '../UserContext';

// IMAGES
import logo from '../images/logo.png'
import logotypeW from '../images/logotypeW.png'
import cart from '../images/cart.png'

export default function AppNavBar() 
{
	
	const {user} = useContext(UserContext);
	console.log({user})

	const [badge, setBadge] = useState(0)
	const [cartArr, setCartArr] = useState([])

	const updateBadge = (e) =>
	{
		setCartArr(JSON.parse(localStorage.getItem('cart')))
		setBadge(cartArr.length)
	}

		

	return (
		<Navbar id="navBar" variant="dark" expand="md">
		     <Container>
		     
		       <Navbar.Brand as = {Link} to= "/" ><img width="60px"  src={logotypeW}/></Navbar.Brand>
		      	{(user.id !== null) ?
		      		<Button className="d-none d-sm-block" variant="outline-light" >
		              {`${user.email}`}
		        	</Button> 
		        	:
		        	<Button variant="outline-white" disabled hidden>
		              {`${user.email}`}
		        	</Button>

		      	}

		      	
		      		
            <Container className="d-none d-sm-block me-3">
            	<InputGroup className="ms-3 me-3 mb-0 ">
            	       <InputGroup.Text id="basic-addon1"><img width="25px" className="img-fluid " src={logo}/></InputGroup.Text>
            	       <Form.Control
            	         placeholder="Search"
            	         aria-label="Username"
            	         aria-describedby="basic-addon1"
            	       />
            	</InputGroup>
            </Container>
              

             <Nav.Link as = {NavLink}  to = "/cart" className="p-0 d-md-none d-sm-block ms-auto">	<img width="42px" className="mx-3" src={cart}/></Nav.Link>

		       <Navbar.Toggle variant="dark" aria-controls="basic-navbar-nav" />
		       
		       <Navbar.Collapse id="basic-navbar-nav">
		        	<Nav className="ms-auto">    

	         		
	         		{(user.isAdmin) ?
	         			<Nav.Link as = {NavLink} to = "/shop" className="mt-1 text-center text-white" >Dashboard</Nav.Link>
	         			:
	         			<Nav.Link as = {NavLink} to = "/shop" className="mt-1 text-center text-white" >Shopping</Nav.Link>
 	         		}
	         		
	         		{/*conditional rendering*/}
	         		{(user.id !== null ) ?
	         			<>

	         			{(user.isAdmin) ?
	         			<Link className= "nav-link mt-1" to="/" hidden> Orders</Link>
	         			:
	         			<Link className= "nav-link mt-1 text-center text-white" to="/orders" > Orders</Link>
 	         			}

	         			
	         			<Link className= "nav-link mt-1 text-center text-white" to="/logout" > Logout</Link>

	         			{(user.isAdmin) ?
	         			<></>
	         			:
	         			<Nav.Link as = {NavLink}  to = "/cart" className="p-0" onClick={(e)=> updateBadge()}>	
	         				<Container>
	         					<Row>
	         						<Col sm={6}>
	         							
	         						<img width="42px" className="mx-3 d-none d-sm-block " src={cart}/> 
	         						
	         						</Col>
	         						<Col sm={6}>
	         							<Badge bg="secondary">{badge}</Badge>
	         						</Col>
	         					</Row>
	         				</Container>  
	         			</Nav.Link> 
	         			}

	         			</>

	         			:
	         			<>
	         			<Nav.Link as = {NavLink} to = "/login" className="mt-1 text-center text-white">Login</Nav.Link>
	         			<Nav.Link as = {NavLink} to = "/register" className="mt-1 text-center text-white">Register</Nav.Link>
	         			</>
	         		}
  
		         		
		         	
		         </Nav>
		       </Navbar.Collapse>

		      

		    

		     </Container>
		   </Navbar>	
	);
}