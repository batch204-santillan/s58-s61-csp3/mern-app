// import Banner from '../components/Banner';
// import Highlights from '../components/Highlights';



import logo from '../images/logo.png'
import header from '../images/header.png'
import car1 from '../images/car1.jpg'
import car2 from '../images/car2.jpg'
import car3 from '../images/car3.jpg'
import featured from '../images/featured.png'

import prod1 from '../images/products/prod1.png'
import prod2 from '../images/products/prod2.png'
import prod3 from '../images/products/prod3.png'
import prod4 from '../images/products/prod4.png'


import {Link, NavLink,} from 'react-router-dom';
import {Carousel, Card, Container, Row, Col} from 'react-bootstrap';


export default function Home ()
{


	return (
		<>
		<Container fluid className="justify-content-center py-5">
			<Row>
				<img img-fluid src={header}/>
			</Row>
		</Container>

		<Carousel className="mt-5 " id="featCarousel">
		     <Carousel.Item interval={6500}>
		       	<img
		       	  className="d-block w-100"
		       	  src={car1}
		       	  alt="First slide"
		       	  id="carousel"
		       	/>
		       	<Carousel.Caption className="d-none d-sm-block caption text-dark bg-white">
		       	  <h3>Dining Room Set</h3>
		       	  <p>A complete dining room set featuring a natural-cut 8-seater dining table with custom upholstery, matched with a one-of-a-kind lighting fixture that will surely liven up any dining experience.</p>
		       	</Carousel.Caption>
		     </Carousel.Item>

		     <Carousel.Item interval={6500}>
		       <img
		         className="d-block w-100"
		         src={car2}
		         alt="Second slide"
		         id="carousel"
		       />
		       <Carousel.Caption className="d-none d-sm-block caption text-dark bg-white">
		         <h3>Living Room Set</h3>
		         <p>Give your living area a make-over with this cozy living set with perfectly matched seats that will surely give a vibrant personality to your space.</p>
		       </Carousel.Caption>
		     </Carousel.Item>

		     <Carousel.Item interval={6500}>
		       <img
		         className="d-block w-100"
		         src={car3}
		         alt="Third slide"
		         id="carousel"
		       />
		       <Carousel.Caption className="d-none d-sm-block caption text-dark bg-white">
		         <h3>Bedroom Set</h3>
		         <p>
		           A minimalist, tropical-inspired bedroom that will give any room a fresh and breezy look.
		         </p>
		       </Carousel.Caption>
		     </Carousel.Item>
		</Carousel>

		<center>
			<img className="mt-5" width="375px" src={featured}/>
		</center>

	   <Carousel className="mt-1 p-3">
	        <Carousel.Item interval={6500}>
	          	<Container>
	          		<Row>
	          			<Col xs={6} md={3}>
	          				<div className="imgCont">
	          					<Link className= "nav-link mt-1 text-center" 
	          					to="/shop/636cd8a1979f547d64d9b2c9" >
		          					<img
		          					  className="d-block w-100"
		          					  src={prod1}
		          					  alt="Second slide"
		          					  id="featuredProd"
		          					/>
		          				</Link>
	          				</div>
	          			</Col>
	          			<Col xs={6} md={3}>
	          				<div className="imgCont">
	          					<Link className= "nav-link mt-1 text-center" 
	          					to="/shop/637230898ee93317a7214780" >
		          					<img
		          					  className="d-block w-100"
		          					  src={prod2}
		          					  alt="Second slide"
		          					  id="featuredProd"
		          					/>
		          				</Link>
	          				</div>
	          			</Col>
	          			<Col xs={6} md={3}>
	          				<div className="imgCont">
	          					<Link className= "nav-link mt-1 text-center" 
	          					to="/shop/636bf1bfd60b51d0c4e0c008" >
		          					<img
		          					  className="d-block w-100"
		          					  src={prod3}
		          					  alt="Second slide"
		          					  id="featuredProd"
		          					/>
		          				</Link>
	          				</div>
	          			</Col>
	          			<Col xs={6} md={3}>
	          				<div className="imgCont">
	          					<Link className= "nav-link mt-1 text-center" 
	          					to="/shop/636df83f7f99833322467913" >
		          					<img
		          					  className="d-block w-100"
		          					  src={prod4}
		          					  alt="Second slide"
		          					  id="featuredProd"
		          					/>
		          				</Link>
	          				</div>
	          			</Col>
	          		</Row>
	          	</Container>
	        </Carousel.Item>

           

	        
	      </Carousel>
              
              
		</>
		)
}
