// IMPORTS

import logo from './images/logo.png'
import './App.css';

  // COMPONENTS
  import AppNavBar from './components/AppNavBar';
  import Footer from './components/Footer';

  // PAGES
  import Home from './pages/Home';
  import Login from './pages/Login';
  import Logout from './pages/Logout';
  import Register from './pages/Register';
  import Shop from './pages/Shop';
  import SpecificProduct from './pages/SpecificProduct';
  import Cart from './pages/Cart';
  import Orders from './pages/Orders';
  import Error from './pages/Error';


  // PACKAGES
  import {UserProvider} from './UserContext';
  import {useState, useEffect} from 'react';
  import {Container} from 'react-bootstrap';
  import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    email: null
  })

  useEffect (() => 
  {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,
    {
      headers: 
      {
        Authorization : `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then (res => res.json())

    .then ( data => {
      // setUser to these values
      console.log(data) 
     if( typeof data._id !== 'undefined')
     {
        setUser (
        {
          id: data._id,
          isAdmin: data.isAdmin,
          email: data.email
        })
     }
     else
     {
        setUser (
        {
          id: null,
          isAdmin: null,
          email: null
        })
     }
    })
  }, [])



  return (
    
      <UserProvider value ={{user, setUser}} >
      <Router>
        <>
        <AppNavBar />
          
        <Container>
          <Switch>
            <Route exact path = "/" component = {Home}/>
            <Route exact path = "/login" component = {Login}/>
            <Route exact path = "/logout" component = {Logout}/>
            <Route exact path = "/register" component = {Register}/>
            <Route exact path = "/shop" component = {Shop}/>
            <Route exact path = "/shop/:productId" component = {SpecificProduct}/>    
            <Route exact path = "/cart" component = {Cart}/>
            <Route exact path = "/orders" component = {Orders}/>
            <Route component = {Error}/>

          </Switch>
        </Container>


        </>
      </Router>  
      <Footer />    
    </UserProvider>
    
    
  );
}

export default App;
